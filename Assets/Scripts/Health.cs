﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int HealthAmount = 100;
    public GameObject Blood;
    private EnemyAI enemyAI;
    void Start()
    {
        enemyAI = GetComponent<EnemyAI>();
    }

    void Update()
    {

    }
    public void ApplyDamage(int BulletDamage)
    {
        HealthAmount -= BulletDamage;

        if (enemyAI.thisCondition == AI_Conditions.Idle)
        {
            enemyAI.thisCondition = AI_Conditions.MovingOnDamage;
        }
        if (HealthAmount <= 0)
        {
            death();
        }
    }
    private void death()
    {
        GameObject blood = Instantiate(Blood, transform.position, Quaternion.Euler(0, 0, Random.Range(0f, 360f)));
        Destroy(gameObject);
        Destroy(blood, 3f);
    }
}
