﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Transform Player;
    private void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Player != null)
        {
            Vector3 cameraTargetPos = new Vector3(Player.position.x, Player.position.y, transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, cameraTargetPos, Time.deltaTime * 2f);
        }
    }
}
