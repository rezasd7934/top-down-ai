﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public AI_Conditions thisCondition;
    public Transform PlayerTransform;
    public List<Sprite> EnemyMovingSprites = new List<Sprite>();
    public float EnemySpeed = 1.5f;
    public float DistanceForMoving = 3.3f, DistanceForAttacking = 0.4f;
    float playerDistance;
    private SpriteRenderer PlayerSR;
    private int movingSpriteIndex;
    public float TimeForChangeSprite = 0.02f;
    private float timer;
    public int enemyDamage;
    public float DamageToPlayerTime;
    private float DamageToPlayerTimeTemp;
    private bool canPatrol = true;
    private bool canFindNewRandomPoint = true;
    public Vector2 PatrolRandomRange;
    private Vector2 randomPointForPatrol;
    public float PatrolTimer = 2f;
    private float PatrolTimerTemp;

    void Start()
    {
        PlayerSR = GetComponent<SpriteRenderer>();
        PatrolTimerTemp = PatrolTimer;
    }

    void Update()
    {
        //determine enemy condition
        if (PlayerController.Player != null)
            playerDistance = Vector2.Distance(this.transform.position, PlayerTransform.position);

        if (playerDistance < DistanceForAttacking)
        {
            thisCondition = AI_Conditions.Attacking;
        }
        else if (playerDistance < DistanceForMoving)
        {
            thisCondition = AI_Conditions.Moving;
        }
        else
        {
            if (thisCondition != AI_Conditions.MovingOnDamage)
                thisCondition = AI_Conditions.Idle;
        }

        //Find New Random Patrol Point
        if (canPatrol && thisCondition == AI_Conditions.Idle)
        {
            if (canFindNewRandomPoint)
            {
                canFindNewRandomPoint = false;
                Vector2 randompoint = Random.insideUnitCircle;
                randomPointForPatrol = (Vector2)transform.position + (randompoint * Random.Range(PatrolRandomRange.x, PatrolRandomRange.y));
            }


            if (Vector2.Distance(transform.position, randomPointForPatrol) > 0.1f)
            {
                RotateEnemy(randomPointForPatrol);
                MoveEnemy();
            }
            else
            {
                PatrolTimerTemp -= Time.deltaTime;
                if (PatrolTimerTemp <= 0)
                {
                    PatrolTimerTemp = PatrolTimer;
                    canFindNewRandomPoint = true;
                }
            }
        }


        //condition of enemy
        if (thisCondition == AI_Conditions.Moving || thisCondition == AI_Conditions.MovingOnDamage)
        {
            RotateEnemy(PlayerTransform.position);
            MoveEnemy();
        }
        else if (thisCondition == AI_Conditions.Attacking && PlayerController.Player != null)
        {
            if (PlayerController.Player == null)
            {
                thisCondition = AI_Conditions.Idle;
            }
            else
            {
                DamageToPlayerTimeTemp += Time.deltaTime;

                if (DamageToPlayerTimeTemp >= DamageToPlayerTime)
                {
                    DamageToPlayerTimeTemp = 0;
                    PlayerController.Player.ApplyDamage(enemyDamage);
                }
            }
        }
        else if (thisCondition == AI_Conditions.Idle)
        {

        }
    }

    public void RotateEnemy(Vector2 DestinationPos)
    {
        Vector2 enemyPos = transform.position;
        Vector2 offset = (DestinationPos - enemyPos).normalized;
        transform.right = offset;
    }

    private void MoveEnemy()
    {
        transform.Translate(Vector2.right * EnemySpeed * Time.deltaTime);

        changeSprites();
    }
    private void changeSprites()
    {
        timer += Time.deltaTime;

        if (timer >= TimeForChangeSprite)
        {
            timer = 0;
            movingSpriteIndex++;
            if (movingSpriteIndex == EnemyMovingSprites.Count)
                movingSpriteIndex = 0;

            PlayerSR.sprite = EnemyMovingSprites[movingSpriteIndex];
        }
    }

}

public enum AI_Conditions
{
    Idle,
    Patroling,
    Moving,
    MovingOnDamage,
    Attacking
}
