﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float PlayerSpeed = 3f;
    public List<Sprite> PlayerMovingSprites = new List<Sprite>();
    private SpriteRenderer PlayerSR;
    private int movingSpriteIndex;
    public float TimeForChangeSprite = 0.02f;
    private float timer;
    private Vector2 MouseOffset;
    public GameObject BulletPrefab;
    public Transform BulletSpawnPoint;
    public float BulletSpawnTime;
    private float BulletSpawnTimeTemp;
    public SpriteRenderer HealthBarSR;
    public Color MaxHealthColor, MinHealthColor;
    public float HealthAmount;
    private float MaxHealthAmount;
    private float healthBarScale;
    public static PlayerController Player;



    void Start()
    {
        PlayerSR = GetComponent<SpriteRenderer>();
        Player = this;
        MaxHealthAmount = HealthAmount;
        healthBarScale = HealthBarSR.transform.localScale.x;
    }
    private void OnDestroy()
    {
        Player = null;
    }

    void Update()
    {
        RotatePlayer();
        MovePlayer();
        checkShooting();
        checkHealthBar();
    }

    private void RotatePlayer()
    {
        Vector2 MousePos = Input.mousePosition;
        Vector2 PlayerPos = transform.position;
        MousePos = Camera.main.ScreenToWorldPoint(MousePos);
        MouseOffset = MousePos - PlayerPos;

        if (MouseOffset != Vector2.zero)
            transform.right = MouseOffset;
    }
    private void MovePlayer()
    {
        if (Input.GetMouseButton(0) && MouseOffset.magnitude > 1.2f)
        {
            transform.Translate(Vector2.right * PlayerSpeed * Time.deltaTime);

            changeSprites();
        }
    }
    private void changeSprites()
    {
        timer += Time.deltaTime;

        if (timer >= TimeForChangeSprite)
        {
            timer = 0;
            movingSpriteIndex++;
            if (movingSpriteIndex == PlayerMovingSprites.Count)
                movingSpriteIndex = 0;

            PlayerSR.sprite = PlayerMovingSprites[movingSpriteIndex];
        }
    }
    private void checkShooting()
    {
        BulletSpawnTimeTemp += Time.deltaTime;
        if (Input.GetMouseButton(1) && BulletSpawnTimeTemp >= BulletSpawnTime)
        {
            BulletSpawnTimeTemp = 0f;
            fireBullets();
        }
    }
    private void fireBullets()
    {
        GameObject bullet = Instantiate(BulletPrefab, BulletSpawnPoint.position, Quaternion.identity) as GameObject;
        bullet.transform.right = -transform.up;
        Destroy(bullet, 4f);
    }
    private void checkHealthBar()
    {
        float healthRatio = HealthAmount / MaxHealthAmount;

        HealthBarSR.color = Color.Lerp(MinHealthColor, MaxHealthColor, healthRatio);
        HealthBarSR.transform.localScale = new Vector3(healthRatio * healthBarScale
        , HealthBarSR.transform.localScale.y, HealthBarSR.transform.localScale.z);
    }
    public void ApplyDamage(int enemyDamage)
    {
        HealthAmount -= enemyDamage;
        if (HealthAmount <= 0)
        {
            death();
        }
    }
    private void death()
    {
        Destroy(gameObject);
    }
}
